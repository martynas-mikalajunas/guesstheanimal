﻿using System.Web.Mvc;
using System.Web.Routing;

namespace GuessTheAnimal.Web
{
	public static class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
					name: "Default",
					url: "{controller}/{action}/{id}",
					defaults: new { controller = "Game", action = "Index", id = UrlParameter.Optional }
			);
			routes.MapRoute(
					name: "Ask",
					url: "{controller}/ask/{id}/{noid}",
					defaults: new { controller = "Game", action = "Ask", id = UrlParameter.Optional, noid = UrlParameter.Optional }
			);
			routes.MapRoute(
					name: "Append",
					url: "{controller}/giveup/{id}/{toyes}",
					defaults: new { controller = "Game", action = "Giveup", id = UrlParameter.Optional, toyes = UrlParameter.Optional }
			);
		}
	}
}