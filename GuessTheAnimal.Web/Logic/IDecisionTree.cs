﻿using System.Linq;
using GuessTheAnimal.Web.DA;

namespace GuessTheAnimal.Web.Logic
{
	public interface IDecisionTree
	{
		Node GetNode(int? id);
		int AddNode(int? parrentNode, short? addToYes, string question, string guess);
	}

	internal class DecisionTree : IDecisionTree
	{
		public Node GetNode(int? nodeId)
		{
			using (var ctx = new DTreeContext())
			{
				if (null != nodeId)
					return ctx.Nodes.FirstOrDefault(n => n.Id == nodeId);

				return ctx.Nodes.OrderBy(n => n.Id).FirstOrDefault();
			}
		}

		public int AddNode(int? parrentNode, short? addToYes, string question, string guess)
		{
			var node = new Node { Guess = guess, Question = question };
			using (var ctx = new DTreeContext())
			{
				ctx.Nodes.Add(node);
				ctx.SaveChanges();
				if (null != parrentNode && null != addToYes)
				{
					var parent = new Node { Id = parrentNode.Value };
					ctx.Nodes.Attach(parent);
					
					if (1 == addToYes)
						parent.YesId = node.Id;
					else 
						parent.NoId = node.Id;

					ctx.SaveChanges();
				}
			}
			return node.Id;
		}
	}
}