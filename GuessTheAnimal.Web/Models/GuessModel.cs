﻿namespace GuessTheAnimal.Web.Models
{
	public class GuessModel
	{
		public string Guess { get; set; }
		public int? NoId { get; set; }
		public int Id { get; set; }

		public bool AppendToYesNode { get { return !NoId.HasValue; } }
	}
}