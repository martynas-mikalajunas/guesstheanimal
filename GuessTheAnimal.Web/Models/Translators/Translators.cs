﻿using GuessTheAnimal.Web.DA;

namespace GuessTheAnimal.Web.Models.Translators
{
	internal static class Translators
	{
		public static QuestionModel AsQuestion(this Node node)
		{
			return null == node ? null : new QuestionModel {Id = node.Id, NoId = node.NoId, Question = node.Question};
		}

		public static GuessModel AsGuess(this Node node)
		{
			return null == node ? null : new GuessModel {Guess = node.Guess, Id = node.Id, NoId = node.YesId};
		}
	}
}