﻿namespace GuessTheAnimal.Web.Models
{
	public class QuestionModel
	{
		public string Question { get; set; }
		public int Id { get; set; }
		public int? NoId { get; set; }
	}
}