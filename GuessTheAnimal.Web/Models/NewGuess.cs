﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GuessTheAnimal.Web.Models
{
	public class NewGuess
	{
		[Required(ErrorMessage = "Reikėtų nurodyti klausimą")]
		[DisplayName("Kokį klausimą reikėjo užduoti?")]
		public string Question { get; set; }
		[Required(ErrorMessage = "Reikėtų nurodyti atsakymą")]
		[DisplayName("Koks tai buvo gyvūnas?")]
		public string Guess { get; set; }
		public int? Id { get; set; }
		public byte? AddToYes { get; set; }
	}
}