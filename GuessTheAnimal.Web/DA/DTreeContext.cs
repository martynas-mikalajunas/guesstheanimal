﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace GuessTheAnimal.Web.DA
{
	internal class DTreeContext : DbContext
	{
		public DTreeContext() :base("tree")
		{
			Database.SetInitializer<DTreeContext>(null);
		}
		public DbSet<Node> Nodes { get; set; }
	}

	public class Node
	{
		public int Id { get; set; }
		public int? NoId { get; set; }
		public string Question { get; set; }
		public string Guess { get; set; }
		public int? YesId { get; set; }
	}
}