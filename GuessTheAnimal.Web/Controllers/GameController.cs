﻿using System;
using System.Web.Mvc;
using GuessTheAnimal.Web.Logic;
using GuessTheAnimal.Web.Models;
using GuessTheAnimal.Web.Models.Translators;

namespace GuessTheAnimal.Web.Controllers
{
	public class GameController : Controller
	{
		private readonly IDecisionTree _tree;

		public GameController() : this(new DecisionTree())
		{
		}

		public GameController(IDecisionTree tree)
		{
			_tree = tree;
		}

		//
		// GET: /Game/
		public ActionResult Index()
		{
			return View();
		}
		
		public ActionResult Start()
		{
			var model = _tree.GetNode(null).AsQuestion();

			if (null == model)
				return RedirectToAction("Giveup");

			return View("Ask", model);
		}

		public ActionResult Guess(int id)
		{
			var model = _tree.GetNode(id).AsGuess();
			return View("Guess", model);
		}

		public ActionResult Ask(int id, int? noid)
		{
			if (null == noid)
				return RedirectToAction("Giveup", new{id, toyes = 0});

			var question = _tree.GetNode(noid).AsQuestion();
			return View("Ask", question);
		}

		public ActionResult End()
		{
			return View();
		}

		public ActionResult Giveup(int? id, byte? toyes)
		{
			return View("Giveup", new NewGuess{Id = id, AddToYes = toyes});
		}

		[HttpPost]
		public ActionResult Giveup(NewGuess newGuess)
		{
			// for unit test only :(
			if (String.IsNullOrEmpty(newGuess.Guess)) ModelState.AddModelError("a", "a");

			if (!ModelState.IsValid)
				return View("Giveup", newGuess);

			_tree.AddNode(newGuess.Id, newGuess.AddToYes, newGuess.Question, newGuess.Guess);
			return RedirectToAction("Index");
		}
	}
}