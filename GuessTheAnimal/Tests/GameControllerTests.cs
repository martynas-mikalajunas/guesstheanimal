﻿using System.Web.Mvc;
using FluentAssertions;
using GuessTheAnimal.Web.Controllers;
using GuessTheAnimal.Web.DA;
using GuessTheAnimal.Web.Logic;
using GuessTheAnimal.Web.Models;
using NSubstitute;
using NUnit.Framework;

namespace GuessTheAnimal.Tests
{
	[TestFixture]
	internal class GameControllerTests
	{
		private GameController _controller;
		private IDecisionTree _tree;

		[SetUp]
		public void SetUp()
		{
			_tree = Substitute.For<IDecisionTree>();
			_controller = new GameController(_tree);
		}

		[Test]
		public void Should_start_with_first_question_in_decision_tree()
		{
			// arrange
			_tree.GetNode(null).Returns(new Node {Id = 1});

			// act
			var result = _controller.Start();

			// assert
			var model = GetModelFromViewVithName<QuestionModel>(result, "Ask");
			model.Id.Should().Be(1, "this is id of root node");
		}

		[Test]
		public void Should_give_up_imediatly_if_start_does_not_find_start_node()
		{
			// arrange

			// act
			var result = _controller.Start();

			// assert
			result.Should().BeOfType<RedirectToRouteResult>("should redirect to input form");
			((RedirectToRouteResult) result).RouteValues["action"].Should().Be("Giveup");
		}

		[Test]
		public void Should_go_to_yes_node_if_guess_was_wrong()
		{
			// arrange
			_tree.GetNode(1).Returns(new Node {Id = 1, YesId = 2});

			// act
			var result = _controller.Guess(1);

			// assert
			var model = GetModelFromViewVithName<GuessModel>(result, "Guess");
			model.NoId.Should().Be(2, "this is yes id from node");
		}

		[Test]
		public void Should_append_to_yes_node_if_guess_was_no_and_there_is_no_no_node()
		{
			// arrange
			_tree.GetNode(1).Returns(new Node {NoId = null});

			// act
			var result = _controller.Guess(1);

			// assert
			var model = GetModelFromViewVithName<GuessModel>(result, "Guess");
			model.AppendToYesNode.Should().Be(true);
		}

		[Test]
		public void Should_try_guess_if_answer_to_question_is_yes()
		{
			// arrange
			const int nodeId = 1;
			var expecedModel = new Node {Guess = "Guess"};

			_tree.GetNode(nodeId).Returns(expecedModel);

			// act
			var result = _controller.Guess(nodeId);

			// assert
			var model = GetModelFromViewVithName<GuessModel>(result, "Guess");
			model.Guess.Should().Be("Guess", "was in db");
		}

		[Test]
		public void Should_display_question_from_no_node_if_users_answer_was_no()
		{
			// arrange
			const int currentNodeid = 1;
			const int noNodeId = 2;

			_tree.GetNode(noNodeId).Returns(new Node{Question = "q", NoId = 3});

			// act
			var result = _controller.Ask(currentNodeid, noNodeId);

			// assert
			var model = GetModelFromViewVithName<QuestionModel>(result, "Ask");
			model.Question.Should().Be("q", "db returned it");
			model.NoId.Should().Be(3, "this is no id from db");
		}

		[Test]
		public void Should_give_up_if_there_is_no_questions_left()
		{
			// arrange

			// act
			var result = _controller.Ask(1,null);

			// assert
			result.Should().BeOfType<RedirectToRouteResult>("should redirect to input form");
			((RedirectToRouteResult) result).RouteValues["action"].Should().Be("Giveup");
			((RedirectToRouteResult) result).RouteValues["id"].Should().Be(1);
			((RedirectToRouteResult) result).RouteValues["toyes"].Should().Be(0);
		}

		[Test]
		public void GiveUp_should_know_last_node_and_on_yes_or_no_branch_it_was()
		{
			// arrange

			// act 
			var result = _controller.Giveup(1, 1);

			// assert
			var model = GetModelFromViewVithName<NewGuess>(result, "Giveup");
			model.Id.Should().Be(1, "this was passed to action");
			model.AddToYes.Should().Be(1, "this was passed to action");
		}

		[Test]
		public void Should_ask_to_append_decision_tree_on_give_up()
		{
			// arrange
			const int parrentNode = 10;
			const string question = "Does it eat carrots?";
			const string guess = "Rabbit";

			// act
			_controller.Giveup(new NewGuess{Question = question, Guess = guess, Id = parrentNode, AddToYes = 1});

			// assert
			_tree.Received().AddNode(parrentNode, 1, question, guess);
		}

		[Test]
		public void Should_not_accept_empty_question_or_guess()
		{
			// arrange
	
			// act
			var result = _controller.Giveup(new NewGuess{Question = "kas?"});

			// assert
			_tree.DidNotReceiveWithAnyArgs().AddNode(null, 0, "", "");
			var model = GetModelFromViewVithName<NewGuess>(result, "Giveup");
			model.Question.Should().Be("kas?");
		}
		#region helpers
		private T GetModelFromViewVithName<T>(ActionResult result, string expectedName) where T : class
		{
			result.Should().BeOfType<ViewResult>();
			((ViewResult) result).ViewName.Should().Be(expectedName, "this is expected view");
			((ViewResult)result).Model.Should().NotBeNull("model should be assigned").And.BeOfType<T>();

			return ((ViewResult)result).Model as T;
		}
		#endregion
	}
}