﻿using System.Linq;
using System.Transactions;
using FluentAssertions;
using GuessTheAnimal.Web.DA;
using GuessTheAnimal.Web.Logic;
using NUnit.Framework;

namespace GuessTheAnimal.Tests
{
	[TestFixture]
	internal class DecisionTreeTests
	{
		private TransactionScope _transaction;
		private DecisionTree _tree;

		[SetUp]
		public void Setup()
		{
			_transaction = new TransactionScope();
			_tree = new DecisionTree();
		}

		[TearDown]
		public void Tierdown()
		{
			_transaction.Dispose();
		}

		[Test]
		public void Should_add_new_new_node()
		{
			// arrange

			// act
			var id = _tree.AddNode(null, null, "q", "g");

			// assert
			using (var ctx = new DTreeContext())
			{
				var node = ctx.Nodes.FirstOrDefault(n => n.Id == id);
				node.Should().NotBeNull("values should be inserted");
				node.Guess.Should().Be("g", "this guess was set");
				node.Question.Should().Be("q", "this question was set");
				node.NoId.Should().NotHaveValue("root node");
				node.YesId.Should().NotHaveValue("root node");
			}
		}

		[Test]
		public void Should_set_new_node_as_parents_no_node_if_asked()
		{
			// arrange
			Node rootNode, noNode;
			InsertTwoNodes(out rootNode, out noNode);

			//act
			var newId = _tree.AddNode(rootNode.Id, 0, "q1", "g1");

			// assert
			using (var ctx = new DTreeContext())
			{
				var node = ctx.Nodes.FirstOrDefault(n => n.Id == rootNode.Id);
				node.NoId.Should().Be(newId, "should be set as no node");
				node.Guess.Should().Be("g", "should not change");
				node.Question.Should().Be("q", "should not change");
			}
		}

		[Test]
		public void Should_set_new_node_as_parents_yes_node_if_asked()
		{
			// arrange
			Node rootNode, noNode;
			InsertTwoNodes(out rootNode, out noNode);

			//act
			var newId = _tree.AddNode(rootNode.Id, 1, "q1", "g1");

			// assert
			using (var ctx = new DTreeContext())
			{
				var node = ctx.Nodes.FirstOrDefault(n => n.Id == rootNode.Id);
				node.YesId.Should().Be(newId, "should be set as no node");
				node.Guess.Should().Be("g", "should not change");
				node.Question.Should().Be("q", "should not change");
			}
		}

		[Test]
		public void GetGuess_should_return_guess()
		{
			// arrange
			Node rootNode, noNode;
			InsertTwoNodes(out rootNode, out noNode);

			// act
			var node = _tree.GetNode(rootNode.Id);

			// assert
			node.Guess.Should().Be("g");
			node.Question.Should().Be("q");
			node.NoId.Should().Be(noNode.Id, "no node");
		}

		[Test]
		public void GetNode_without_id_should_return_start_node()
		{
			// arrange
			Node rootNode, noNode;
			InsertTwoNodes(out rootNode, out noNode);

			// act
			var startNode = _tree.GetNode(null);

			// assert
			startNode.Should().NotBeNull("start node should exist");
			startNode.Id.Should().BeLessOrEqualTo(rootNode.Id, "root node should be the first in db");
		}

		#region private
		private static void InsertTwoNodes(out Node rootNode, out Node noNode)
		{
			rootNode = new Node {Guess = "g", Question = "q"};
			noNode = new Node {Guess = "g1", Question = "q1"};
			using (var ctx = new DTreeContext())
			{
				ctx.Nodes.Add(rootNode);
				ctx.Nodes.Add(noNode);
				ctx.SaveChanges();

				rootNode.NoId = noNode.Id;
				ctx.SaveChanges();
			}
		}
		#endregion

	}
}