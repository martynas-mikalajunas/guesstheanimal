﻿create table dbo.Nodes
(
	Id int not null identity(1,1) constraint pk_nodes primary key,
  Noid int null,
  YesId int null,
  Question nvarchar(500) not null,
  Guess nvarchar(500) not null
)
